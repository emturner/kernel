// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! When originating a _WebAssembly Smart Rollup_ on Tezos, you must specify the full kernel
//! (as hex-encoded WebAssembly) to be executed. Since this operation takes place on Layer-1,
//! this places a limit of *32KB* on the size of the origination kernel.
//!
//! Most useful kernels will be significantly larger than this, however, due to requiring
//! certain runtime attributes such as _allocation_, _formatting_, and _cryptographic
//! verification_.
//!
//! Therefore, so-called _installer kernels_ are provided - that are small enough to be
//! used as an origination kernel, that contain logic to install the kernel actually desired
//! by the rollup originator. This is done through the _kernel upgrade mechanism_.
#![cfg_attr(not(test), no_std)]
#![deny(rustdoc::all)]
#![allow(unused)]

use core::panic::PanicInfo;

#[macro_use]
extern crate kernel;

pub mod reveal_installer;

#[cfg(feature = "preimage-installer")]
pub mod preimage_installer {
    //! The preimage installer installs the kernel preimage from the root hash.
    use crate::reveal_installer::install_kernel;
    use debug::debug_str;
    use host::rollup_core::{RawRollupCore, PREIMAGE_HASH_SIZE};

    /// The preimage root hash that the installer will request.
    ///
    /// This is a placeholder value, it is possible to compile the installer kernel,
    /// and replace this with whatever root hash you choose, that corresponds to the
    /// kernel you wish to install.
    pub const ROOT_PREIMAGE_HASH: &[u8; PREIMAGE_HASH_SIZE * 2] =
        b"1acaa995ef84bc24cc8bb545dd986082fbbec071ed1c3e9954abea5edc441ccd3a";

    kernel_entry!(reveal_installer);

    /// Attempts to install the kernel corresponding to [ROOT_PREIMAGE_HASH].
    pub fn reveal_installer<Host: RawRollupCore>(host: &mut Host) {
        if let Err(err) = install_kernel(host, ROOT_PREIMAGE_HASH) {
            debug_str!(Host, "Installation failed:");
            debug_str!(Host, err);
        } else {
            debug_str!(Host, "Kernel installed.")
        }
    }
}

/// Panic handler used when targetting wasm.
///
/// Any error when installing a kernel is fatal, therefore we
/// handle panics by panicking again, which aborts execution.
#[cfg_attr(target_arch = "wasm32", panic_handler)]
fn panic(_info: &PanicInfo) -> ! {
    panic!()
}
