// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Defines operations over kernel memory - persisted in RAM between yields.

/// The memory used by the transactions kernel.
pub mod tx {
    pub use kernel_core::memory::{Account, Accounts, Memory};
}
